INT_TOKEN
	int
ID_TOKEN
	fact
OPEN_PARA_TOKEN
	(
INT_TOKEN
	int
ID_TOKEN
	x
CLOSE_PARA_TOKEN
	)
OPEN_CURLY_TOKEN
	{
IF_TOKEN
	if
OPEN_PARA_TOKEN
	(
ID_TOKEN
	x
GREATER_TOKEN
	>
NUM_TOKEN
	1
CLOSE_PARA_TOKEN
	)
RETURN_TOKEN
	return
ID_TOKEN
	x
MULTIPLY_TOKEN
	*
ID_TOKEN
	fact
OPEN_PARA_TOKEN
	(
ID_TOKEN
	x
MINUS_TOKEN
	-
NUM_TOKEN
	1
CLOSE_PARA_TOKEN
	)
SEMICOLON_TOKEN
	;
ELSE_TOKEN
	else
RETURN_TOKEN
	return
NUM_TOKEN
	1
SEMICOLON_TOKEN
	;
CLOSE_CURLY_TOKEN
	}
VOID_TOKEN
	void
ID_TOKEN
	main
OPEN_PARA_TOKEN
	(
VOID_TOKEN
	void
CLOSE_PARA_TOKEN
	)
OPEN_CURLY_TOKEN
	{
INT_TOKEN
	int
ID_TOKEN
	x
SEMICOLON_TOKEN
	;
ID_TOKEN
	x
ASSIGN_TOKEN
	=
ID_TOKEN
	read
OPEN_PARA_TOKEN
	(
CLOSE_PARA_TOKEN
	)
SEMICOLON_TOKEN
	;
IF_TOKEN
	if
OPEN_PARA_TOKEN
	(
ID_TOKEN
	x
GREATER_TOKEN
	>
NUM_TOKEN
	0
CLOSE_PARA_TOKEN
	)
ID_TOKEN
	write
OPEN_PARA_TOKEN
	(
ID_TOKEN
	fact
OPEN_PARA_TOKEN
	(
ID_TOKEN
	x
CLOSE_PARA_TOKEN
	)
CLOSE_PARA_TOKEN
	)
SEMICOLON_TOKEN
	;
CLOSE_CURLY_TOKEN
	}
INT_TOKEN
	int
ID_TOKEN
	sum
OPEN_PARA_TOKEN
	(
INT_TOKEN
	int
ID_TOKEN
	a
COMMA_TOKEN
	,
INT_TOKEN
	int
ID_TOKEN
	b
CLOSE_PARA_TOKEN
	)
OPEN_CURLY_TOKEN
	{
RETURN_TOKEN
	return
ID_TOKEN
	a
PLUS_TOKEN
	+
ID_TOKEN
	b
SEMICOLON_TOKEN
	;
CLOSE_CURLY_TOKEN
	}
INT_TOKEN
	int
ID_TOKEN
	getFirstElement
OPEN_PARA_TOKEN
	(
INT_TOKEN
	int
OPEN_BRACKET_TOKEN
	[
CLOSE_BRACKET_TOKEN
	]
ID_TOKEN
	arr
CLOSE_PARA_TOKEN
	)
OPEN_CURLY_TOKEN
	{
RETURN_TOKEN
	return
ID_TOKEN
	arr
OPEN_BRACKET_TOKEN
	[
NUM_TOKEN
	0
CLOSE_BRACKET_TOKEN
	]
SEMICOLON_TOKEN
	;
CLOSE_CURLY_TOKEN
	}
INT_TOKEN
	int
ID_TOKEN
	isInMiddle
OPEN_PARA_TOKEN
	(
INT_TOKEN
	int
ID_TOKEN
	a
COMMA_TOKEN
	,
INT_TOKEN
	int
ID_TOKEN
	b
COMMA_TOKEN
	,
INT_TOKEN
	int
ID_TOKEN
	c
CLOSE_PARA_TOKEN
	)
OPEN_CURLY_TOKEN
	{
RETURN_TOKEN
	return
OPEN_PARA_TOKEN
	(
ID_TOKEN
	a
LESS_EQ_TOKEN
	<=
ID_TOKEN
	b
CLOSE_PARA_TOKEN
	)
OPEN_PARA_TOKEN
	(
ID_TOKEN
	c
GREATER_EQ_TOKEN
	>=
ID_TOKEN
	b
CLOSE_PARA_TOKEN
	)
SEMICOLON_TOKEN
	;
CLOSE_CURLY_TOKEN
	}
INT_TOKEN
	int
ID_TOKEN
	isNotEqual
OPEN_PARA_TOKEN
	(
INT_TOKEN
	int
ID_TOKEN
	a
COMMA_TOKEN
	,
INT_TOKEN
	int
ID_TOKEN
	b
CLOSE_PARA_TOKEN
	)
OPEN_CURLY_TOKEN
	{
RETURN_TOKEN
	return
ID_TOKEN
	a
NOT_EQUIVALENT_TOKEN
	!=
ID_TOKEN
	b
SEMICOLON_TOKEN
	;
CLOSE_CURLY_TOKEN
	}
EOF_TOKEN
	￿
