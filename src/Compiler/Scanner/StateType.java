package Compiler.Scanner;

/**
  * Provides an enum defining the states our CMinusScanner can be in.
  * 
  * @author James Osborne and Jeremy Tiberg
  * File: StateType.java
  * Created:  29 Jan 2018
  * Description: This enum covers all the different states our lexer can have.
  **/

public enum StateType {
    START,
    DONE,
    IN_ID,
    IN_NUM,
    IN_MULTIPLY,
    IN_DIVIDE,
    IN_ASSIGN,
    IN_NOT,
    IN_LT,
    IN_GT,
    IN_COMMENT
}
