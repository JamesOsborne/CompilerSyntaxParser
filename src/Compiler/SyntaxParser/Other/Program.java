package Compiler.SyntaxParser.Other;

import Compiler.SyntaxParser.Decl.Decl;
import java.util.ArrayList;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Program.java
  **/
public class Program {
    private ArrayList<Decl> declarationList;
    
    public Program() {
        this(new ArrayList<Decl>());
    }
    
    public Program(ArrayList<Decl> declarationList) {
        this.declarationList = declarationList;
    }
    
    public void print() {
        int indent = 0;
        int childIndent = indent + 4;
        
        printHelper(indent, "Program {");
        
        for (Decl decl : this.declarationList) {
            decl.print(childIndent);
        }
        
        printHelper(indent, "}");
    }
}
